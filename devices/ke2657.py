import time
from .pyvisa_device import device, device_error


class ke2657(device):
    """
    Keithley 2657A source meter.

    Example:
    -------------
    dev = ke2657A(address=24)
    dev.print_idn()
    dev.set_source('voltage')
    dev.set_voltage(10)
    dev.set_sense('current')
    dev.set_current_limit(0.00005)
    dev.set_output_on()
    print dev.read_current()
    print dev.read_voltage()
    print dev.read_resistance()
    dev.set_output_off()
    dev.reset()
    """

    def __init__(self, address):
        device.__init__(self, address=address)
        self.ctrl.write("smua.reset()")

    def print_idn(self, debug=0):
        if debug == 1:
            self.logging.info("Query ID.")
        self.logging.info(self.ctrl.query("*IDN?"))
        return 0

    def get_idn(self):
        return self.ctrl.query("*IDN?")

    def reset(self, debug=0):
        if debug == 1:
            self.logging.info("Reseting device.""")
        self.ctrl.write("smua.reset()")
        return 0



    # Output functions
    # ---------------------------------

    def set_output_on(self, debug=0):
        if debug == 1:
            self.logging("Set output on.")
        self.ctrl.write("smua.source.autorangev = 1")
        self.ctrl.write("smua.source.output = 1")
        return 0

    def set_output_off(self, debug=0):
        if debug == 1:
            self.logging("Set output off.")
        self.ctrl.write("smua.source.output = 0")
        return 0

    def check_output(self, debug=0):
        if debug == 1:
            self.logging("Check output.")
        return self.ctrl.query("print(smua.source.output)")


    # Set attribute functions
    # ---------------------------------

    def set_voltage(self, val, debug=0):
        if debug == 1:
            self.logging.info("Setting voltage to %.2fV." % val)
        self.ctrl.write("smua.source.levelv = %.2f" % val)
        return 0

    def set_current(self, val, debug=0):
        if debug == 1:
            self.logging.info("Setting current to %.6fA." % val)
        self.ctrl.write("smua.source.leveli = %.2f" % val)
        return 0

    def ramp_voltage(self, val, debug=0):
        now = int(round(self.read_voltage()))
        if debug == 1:
            self.logging.info("Ramping voltage from %.2f V to %.2f V." % (now, val))
        if now > val:
            for v in range(now, int(round(val)), -25):
                self.ctrl.write("smua.source.levelv = %.2f" % v)
                time.sleep(1)
                if debug == 1:
                    print self.read_voltage()
            self.ctrl.write("smua.source.levelv = %.2f" % val)
        else:
            for v in range(now, int(round(val)), +25):
                self.ctrl.write("smua.source.levelv = %.2f" % v)
                time.sleep(1)
                if debug == 1:
                    print self.read_voltage()
            self.ctrl.write("smua.source.levelv = %.2f" % val)
        return 0

    def check_compliance(self, debug=0):
        if debug == 1:
            self.logging.info("Checking for compliance.")
        val = self.ctrl.query("print(smua.source.compliance)")
        if val == "true":
            return 1
        else:
            return 0

    def set_sense(self, prop, debug=0):
        self.logging.info("You do not need to choose sense on this device.")
        return 0

    def set_source(self, prop, debug=0):
        if prop == 'voltage':
            if debug == 1:
                self.logging.info("Setting source to voltage.")
            self.ctrl.write("smua.source.func = 1")
        elif prop == 'current':
            if debug == 1:
                self.logging.info("Setting source to current.")
            self.ctrl.write("smua.source.func = 0")
        else:
            self.logging.info("Choosen property cannot be supplied by this device.")
            return -1

    def set_voltage_limit(self, val, debug=0):
        if debug == 1:
            self.logging.info("Setting voltage limit to %.2fV." % val)
        self.ctrl.write('smua.source.limitv = %.2E' % val)
        return 0

    def set_current_limit(self, val, debug=0):
        if debug == 1:
            self.logging.info("Setting current limit to %.6fA." % val)
        self.ctrl.write("smua.source.limiti = %.2E" % val)
        return 0

    def set_nplc(self, val, debug=0):
        if debug == 1:
            self.logging.info("Setting NPLC to %.2f." % val)
        self.ctrl.write("smua.measure.nplc = %.2f" % val)
        return 0

    def check_voltage_limit(self, debug=0):
        if debug == 1:
            self.logging.info("Checking for voltage limit.")
        return float(self.ctrl.query("print(smua.source.limitv)"))

    def check_current_limit(self, debug=0):
        if debug == 1:
            self.logging.info("Checking for current limit.")
        return float(self.ctrl.query("print(smua.source.limiti)"))

    def set_interlock_on(self, debug=0):
        self.logging.info("No interlock implemented.")
        return 0

    def set_interlock_off(self, debug=0):
        self.logging.info("No interlock implemented.")
        return 0

    def set_terminal(self, term, debug=0):
        self.logging.info("This device has only one terminal.")
        return 0


    # Read attribute functions
    # ---------------------------------

    def read_voltage(self):
        val = self.ctrl.query("print(smua.measure.v())")
        return float(val)

    def read_current(self):
        val = self.ctrl.query("print(smua.measure.i())")
        return float(val)
