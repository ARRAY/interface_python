import time
from .pyvisa_device import device, device_error


class tek70000(device):
    """
    Tektronix DSA70000 oscilloscope.

    Example:
    -------------
    dev = tek70000(address=GPIB1::1::INSTR)
    print(dev.get_idn())
    dev.print_idn()
    dev.lock_panel()
    print(dev.get_setup())
    print(dev.get_channel_state(2))
    dev.set_channel_state(2, 1)
    print(dev.get_channel_state(2))
    print(dev.get_all_channels())

    dev.unlock_panel()

    """

    def __init__(self, address):
        device.__init__(self, address=address)
        self.ctrl.write("*RST")
        self.ctrl.write("CLEAR")

    def print_idn(self, debug=0):
        if debug == 1:
            self.logging.info("Query ID.")
        self.logging.info(self.ctrl.query("*IDN?"))
        return 0

    def get_idn(self, debug=0):
        if debug == 1:
            self.logging.info("Get identificiation.")
        return self.ctrl.query("*IDN?")

    def list_instrument_settings(self, debug=0):
        if debug == 1:
            self.logging.info("List instrument settings.")
        return self.ctrl.query("*LRN?")

    def clear(self, debug=0):
        if debug == 1:
            self.logging.info("Clear device.")
        return self.ctrl.query("CLEAR")

    def reset(self, debug=0):
        if debug == 1:
            self.logging.info("Reseting device.")
        self.ctrl.write("*RST")
        return 0



    # Setup functions
    # ---------------------------------

    def lock_panel(self):
        self.ctrl.write("LOCK ALL")
        return 0

    def unlock_panel(self):
        self.ctrl.write("LOCK NONE")
        return 0

    def set_channel_state(self, ch, state):
        self.ctrl.write("SEL:CH"+ str(ch) + " " + str(state))
        return 0

    def get_channel_state(self, ch):
        return self.ctrl.query("SEL:CH"+ str(ch) +"?")

    def set_all_channels(self, nch, state):
        ch_state = []
        for ch in range(nch):
            self.ctrl.write("SEL:CH"+ str(ch+1) + " " + str(state))
        return 0

    def get_all_channels(self, nch):
        ch_state = []
        for ch in range(nch):
            ch_state.append(int(self.ctrl.query("SEL:CH"+ str(ch+1) +"?")))
        return ch_state

    def get_trigger_state(self):
        return self.ctrl.query("TRIGGER:STATE?")

    def get_aquisition_state(self):
        return self.ctrl.query("ACQ:STATE?")

    def get_sampling_rate(self):
        return self.ctrl.query("HORIZONTAL:MODE:SAMPLERATE?")

    def get_sampling_length(self):
        return self.ctrl.query("HORIZONTAL:RECORDLENGTH?")

    def get_sampling_duration(self):
        return self.ctrl.query("HORIZONTAL:ACQDURATION?")

    def set_sampling_rate(self, rate, debug=0):
        if debug == 1:
            self.logging.info("Setting sampling rate to %.2e samples per second." % (rate))
        self.ctrl.write("HORIZONTAL:MODE MANUAL")
        self.ctrl.write("HORIZONTAL:MODE:SAMPLERATE %.2e" % rate)
        return 0

    def set_sampling_length(self, length, debug=0):
        if debug == 1:
            self.logging.info("Setting sampling length to %d samples." % (length))
        self.ctrl.write("HORIZONTAL:RECORDLENGTH %d" % length)
        return 0


    # Complex functions
    # ---------------------------------

    def set_trigger_edge(self, ch, val, debug=0):
        if debug == 1:
            self.logging.info("Setting trigger to %.2f V of the rising edge on chanel %d." % (vsl, ch))
        self.ctrl.write("TRIGGER:MODE NORM")
        self.ctrl.write("TRIGGER:EDGE:COUPLING DC")
        self.ctrl.write("TRIGGER:EDGE:SLOPE RISE")
        self.ctrl.write("TRIGGER:SOURCE " + str(ch))
        self.ctrl.write("TRIGGER:LEVEL " + str(val))
        if debug == 1:
            return self.ctrl.query("TRIGGER?")
        else:
            return 0

    def setup_osci(self, debug=0):
        if debug == 1:
            self.logging.info("General setup.")

        ## general IO
        self.ctrl.write("VERBOSE OFF")
        self.ctrl.write("HARDCOPY:LAYOUT PORTRAIT")
        self.ctrl.write("PREVIEW 0")
        self.ctrl.write("INKSAVER 0")
        self.ctrl.write("HARDCOPY:PALETTE NORMAL")
        self.ctrl.write("SAVE:IMAG:FILEF PNG")
        return 0

    def get_curves_raw(self, ch_list):
        if self.ctrl.query("TRIGGER:STATE?") != 'READY':
            dat_raw = []
            for ch in ch_list:
                self.ctrl.write("DATA:SOURCE CH" + str(ch+1))
                # DATa:ENC
                self.ctrl.write("CURVE?")
                dat_raw.append(self.ctrl.read_raw())
        return dat_raw

    def convert_curves(self, data_raw):
        data_conv = []
        for d in data_raw:
            d = d[headerLen:(int(recLen)-1)]            # strip the header
            d = unpack('%sB' % len(d), d)               # unpack
            data_conv.append(d)
        return data_conv

    def get_curves_conv(self, ch_list):
        if self.ctrl.query("TRIGGER:STATE?") != 'READY':
            chx = []
            chy = []
            for ch in ch_list:
                self.ctrl.write("DATA:SOURCE CH" + str(ch+1))
                self.ctrl.write("CURVE?")

                ## get raw data
                dr = self.ctrl.read_raw()

                ## strip header
                dr = dr[headerLen:(int(recLen)-1)]

                ## unpack and convert
                dc = unpack('%sB' % len(dr), dr)

                # convert to physical units
                x = []
                y = []
                for i in range(0, len(d)):
                    x.append((i-(len(d)*(HPos/100)))* xincr + HDelay)
                    y.append(((d[i]-yoff) * ymult) + yzero)
                chx.append(x)
                chy.append(y)

        return chx, chy

    def get_hardcopy(self):
        if self.ctrl.query("TRIGGER:STATE?") != 'READY':
            while True:
                try:
                    self.ctrl.write("HARDCOPY START")
                    sleep(.01)
                    data = self.ctrl.read()
                    self.ctrl.query('*OPC?')
                    break
                except visa.VisaIOError:
                    break

            ## save
            f = open(filepath + filename + str(filenumber)+'.'+ fileformat, 'wb')
            f.write(data)
            f.close()




    def aquire_frames(self, nframes, ch_list, debug=0):

        frame = 0
        ch_list = [1, 3]

        self.ctrl.write("TRIGGER:MODE NORM")
        self.ctrl.write("ACQ:STOPA SEQ")

        ## loop frames
        while frame < nframes:
            self.ctrl.write("ACQ:STATE RUN")
            wait = 1
            loop_time = time.time()
            print(frame)

            # wait until trigger ready
            while (wait == 1):
                wait = int(self.ctrl.query("ACQ:STATE?"))
                print(wait)
            #
            #     # capture the trigger time
            #     if wait == 0:
            #         trig_time = time.time()
            #         self.ctrl.write("TRIGGER FORCE")
            #         print("Trigger forced at " + str(time.ctime(trig_time)) + '\n')

            # get curve
            # dat = get_curves_raw(ch_list)

            # safe curve


            # increment the capture number
            frame += 1
            # filenumber += 1


dev = tek70000(address="GPIB1::1::INSTR")
print(dev.get_idn())
dev.lock_panel()

dev.set_all_channels(nch=4, state=0)
print(dev.get_all_channels(nch=4))
dev.set_all_channels(nch=4, state=1)
# dev.set_channel_state(ch=2, state=0)
print(dev.get_all_channels(nch=4))

dev.set_sampling_rate(25e9)
dev.set_sampling_length(25e5)
print(dev.get_sampling_rate())
print(dev.get_sampling_length())
print(dev.get_sampling_duration())

# dev.aquire_frames(1, [1,2])
# print(dev.ctrl.write("ACQ:STOPA SEQ"))
print(dev.ctrl.write("ACQ:STATE RUN"))
print(dev.ctrl.write("DATA:SOURCE CH2"))
# print(dev.ctrl.query("DATA:SOURCE?"))
print(dev.ctrl.write("DATA:ENCdg RIBinary"))
# print(dev.ctrl.query("DATA:ENCdg?"))
print(dev.ctrl.write("DATA:START 0"))
print(dev.ctrl.write("DATA:STOP 1e9"))
# print(dev.ctrl.query("DATA:START?"))
# print(dev.ctrl.query("DATA:STOP?"))
print(dev.ctrl.write("SAVE:WAVEFORM:FILEFORMAT INTERNAL"))
# print(dev.ctrl.query("SAVE:WAVEFORM:FILEFORMAT?"))
print(dev.ctrl.write("WFMOutpre:BYT_Nr 8"))
# print(dev.ctrl.query("WFMOutpre:BYT_Nr?"))


print(dev.ctrl.write("TRIGGER:A:MODE NORMAL"))
print(dev.ctrl.query("TRIGGER:A:MODE?"))


# print(dev.ctrl.write("SAVEON:FILE:DEST 'C:\TekScope\waveforms\'"))
# print(dev.ctrl.write("SAVEON:FILE:NAME 'test'"))
# print(dev.ctrl.write("SAVEON:FILE:AUTOINC ON"))
# print(dev.ctrl.query("SAVEON:FILE:AUTOINC?"))
# print(dev.ctrl.query('SAVEON:FILE:DEST?'))
# print(dev.ctrl.write("SAVEON:FILE:TYPE AUTO"))
# print(dev.ctrl.write("SAVEON:TRIG ON"))
# print(dev.ctrl.write("SAVEON:WAVEform ON"))
# print(dev.ctrl.query("SAVEON:WAVEform?"))
# print(dev.ctrl.write("SAVEON:IMAGE ON"))
# print(dev.ctrl.query("SAVEON:IMAGE?"))

print(dev.ctrl.query("ACQ:STATE?"), dev.ctrl.query("TRIGGER:STATE?"))
t0 = time.time()
for i in range(3):
    dev.ctrl.write("TRIGGER FORCE")
    time.sleep(0.01)
    while (dev.ctrl.query("TRIGGER:STATE?") != "READY\n"):
        time.sleep(0.01)
    # dat = dev.ctrl.query("CURVE?")
    print(dev.ctrl.write("SAVE:WAVEFORM ALL"))
    print(time.time() - t0, dev.ctrl.query("ACQ:STATE?"), dev.ctrl.query("TRIGGER:STATE?"))
