# ============================================================================
# File: test02_scan_iv.py
# ------------------------------
#
# Notes:
#
# Layout:
#   configure and prepare
#   for each voltage:
#       set voltage
#       for each channel:
#           set voltage
#           measure voltage, current, total current
#   finish
#
# Status:
#   works well
#
# ============================================================================

import time
import logging
import numpy as np
from .measurement import measurement
from devices import ke2410 # power supply
from devices import ke6510 # volt meter
from devices import switchcard # switch



class test02_scan_iv(measurement):
    """Measurement of I-V curves for individual cells across the wafer matrix."""

    def initialise(self):
        self.logging.info("\t")
        self.logging.info("------------------------------------------")
        self.logging.info("IV Scan")
        self.logging.info("------------------------------------------")
        self.logging.info(self.__doc__)
        self.logging.info("\t")

        self._initialise()
        self.pow_supply_address = 'GPIB0::16::INSTR' # address of the power supply
        self.volt_meter_address = 'TCPIP0::169.254.145.52::inst0::INSTR'    # address of the multi meter
        self.switch_address = 'COM8'    # serial port of switch card

        self.lim_cur = 0.005           # compliance in [A]
        self.lim_vol = 1000             # compliance in [V]

        self.cell_list = np.loadtxt('config/map8Inch192.txt', dtype=int)[:,1]
        self.volt_list = np.loadtxt('config/voltagesIVneg_detailed.txt', dtype=int)

        self.delay_vol = 5              # delay between setting voltage and executing measurement in [s]
        self.delay_ch = 0.3             # delay between setting channel and executing measurement in [s]

        self.flag_list = np.zeros(len(self.cell_list))  # list of cells to skip



    def execute(self):

        ## Set up power supply
        pow_supply = ke2410(self.pow_supply_address)
        pow_supply.reset()
        pow_supply.set_source('voltage')
        pow_supply.set_sense('current')
        pow_supply.set_current_limit(self.lim_cur)
        pow_supply.set_voltage(0)
        pow_supply.set_terminal('rear')
        pow_supply.set_interlock_off()
        pow_supply.set_output_on()

        ## Set up volt meter
        volt_meter = ke6510(self.volt_meter_address)
        volt_meter.reset()
        volt_meter.setup_ammeter()
        volt_meter.set_nplc(5)

        # Set up switch
        switch = switchcard(self.switch_address)
        switch.reboot()
        switch.set_measurement_type('IV')
        switch.set_display_mode('OFF')

        ## Check settings
        lim_vol = pow_supply.check_voltage_limit()
        lim_cur = pow_supply.check_current_limit()
        temp_pc = switch.get_probecard_temperature()
        temp_sc = switch.get_matrix_temperature()
        humd_pc = switch.get_probecard_humidity()
        humd_sc = switch.get_matrix_humidity()
        type_msr = switch.get_measurement_type()
        type_disp = switch.get_display_mode()

        ## Header
        hd = [
            'Scan IV\n',
            'Measurement Settings:',
            'Power supply voltage limit:      %8.2E V' % lim_vol,
            'Power supply current limit:      %8.2E A' % lim_cur,
            'Voltage delay:                   %8.2f s' % self.delay_vol,
            'Channel delay:                   %8.2f s' % self.delay_ch,
            'Probecard temperature:           %8.1f C' % temp_pc,
            'Switchcard temperature:          %8.1f C' % temp_sc,
            'Probecard humidity:              %8.1f %%' % humd_pc,
            'Switchcard humidity:             %8.1f %%' % humd_sc,
            'Switchcard measurement setting:  %s' % type_msr,
            'Switchcard display setting:      %s' % type_disp,
            '\n\n',
            'Nominal Voltage [V]\tChannel [-]\tCurrent [nA]\tError [nA]\tTotal Current [nA]\tMeasured Voltage [V]\tTime [s]\tTemperature [C]\tHumidity [%]'
        ]

        ## Print Info
        for line in hd[1:-2]:
            self.logging.info(line)
        self.logging.info("\t")
        self.logging.info("\t")
        self.logging.info(hd[-2])
        self.logging.info(hd[-1])
        self.logging.info("-" * int(1.2 * len(hd[-1])))

        ## Prepare
        out = []

        ## Loop over voltages
        t0 = time.time()
        try:
            for v in self.volt_list:
                switch.short_all()
                time.sleep(self.delay_ch)
                pow_supply.ramp_voltage(v)
                time.sleep(self.delay_vol)

                if int(pow_supply.check_compliance()) != 0:
                    self.logging.error("Power supply in compliance, aborting measurement.")
                    break

                j = 0
                for c in self.cell_list:

                    ## Only measure unflagged cells
                    if self.flag_list[j] == 0:

                        ## Through away first measurements after voltage change
                        if j == 0:
                            switch.open_channel(c)
                            for k in range(3):
                                volt_meter.read_current()
                                pow_supply.read_current()
                                time.sleep(0.001)

                        ## Go on with normal measurement
                        switch.open_channel(c)
                        time.sleep(self.delay_ch)

                        cur_tot = pow_supply.read_current()
                        vol = pow_supply.read_voltage()

                        t = time.time() - t0
                        hum = switch.get_matrix_humidity()
                        temp = switch.get_matrix_temperature()

                        measurements = np.array([volt_meter.read_current() for _ in range(5)])
                        i = np.mean(measurements, axis=0)
                        di = np.std(measurements, axis=0)

                        ## Flag cell if current too large
                        if abs(i) > 1E-4:
                            self.flag_list[j] = 1

                    ## Handle flagged cells
                    else:
                        cur_tot = pow_supply.read_current()
                        vol = pow_supply.read_voltage()

                        i = np.nan
                        di = np.nan

                    j += 1

                    line = [v, j, i*1e9, di*1e9, cur_tot*1e9, vol, t, temp, hum]
                    out.append(line)
                    self.logging.info("{:<5.2E}\t{: <5d}\t{: <8.3E}\t{: <8.3E}\t{: <5.2E}\t{: <5.2E}\t{: <8.1f}\t{: <5.1f}\t{: <5.1f}".format(*line))

        except KeyboardInterrupt:
            switch.short_all()
            pow_supply.ramp_voltage(0)
            self.logging.error("Keyboard interrupt. Ramping down voltage and shutting down.")

        ## Close connections
        switch.short_all()
        pow_supply.ramp_voltage(0)
        time.sleep(15)
        pow_supply.set_interlock_off()
        pow_supply.set_output_off()
        pow_supply.reset()
        volt_meter.reset()

        ## Save and print
        self.logging.info("\n")
        self.save_list(out, "iv.dat", fmt="%.5E", header="\n".join(hd))
        self.print_graph(np.array(out)[:, 1], np.array(out)[:, 4], np.array(out)[:, 4]*0.001, \
            'Channel Nr. [-]', 'Total Current [A]', 'All Channels ' + self.id, fn="iv_all_channels_%s.png" % self.id)
        self.print_graph(np.array(out)[:, 1], np.array(out)[:, 2], np.array(out)[:, 3], \
            'Channel Nr. [-]', 'Leakage Current [A]',  'IV All Channels ' + self.id, fn="iv_all_channels_%s.png" % self.id)
        if 1:
            ch = 1
            self.print_graph(np.array([val for val in out if (val[1] == ch)])[:, 0], \
                np.array([val for val in out if (val[1] == ch)])[:, 2], \
                np.array([val for val in out if (val[1] == ch)])[:, 3], \
                'Bias Voltage [V]', 'Leakage Current [A]', 'IV ' + self.id, fn="iv_channel_%d_%s.png" % (ch, self.id))
        if (10 in np.array(out)[:, 0]):
            self.print_graph(np.array([val for val in out if (val[0] == 10)])[:, 1], \
                np.array([val for val in out if (val[0] == 10)])[:, 2], \
                np.array([val for val in out if (val[0] == 10)])[:, 3], \
                'Channel Nr. [-]', 'Leakage Current [A]', 'IV ' + self.id, fn="iv_all_channels_10V_%s.png" % self.id)
            self.print_graph(np.array([val for val in out if (val[0] == 10)])[:, 1], \
                np.array([val for val in out if (val[0] == 10)])[:, 4], \
                np.array([val for val in out if (val[0] == 10)])[:, 4]*0.001, \
                'Channel Nr. [-]', 'Total Current [A]', 'IV ' + self.id, fn="iv_total_current_all_channels_10V_%s.png" % self.id)
        if (100 in np.array(out)[:, 0]):
            self.print_graph(np.array([val for val in out if (val[0] == 100)])[:, 1], \
                np.array([val for val in out if (val[0] == 100)])[:, 2], \
                np.array([val for val in out if (val[0] == 100)])[:, 3], \
                'Channel Nr. [-]', 'Leakage Current [A]', 'IV ' + self.id, fn="iv_all_channels_100V_%s.png" % self.id)
            self.print_graph(np.array([val for val in out if (val[0] == 100)])[:, 1], \
                np.array([val for val in out if (val[0] == 100)])[:, 4], \
                np.array([val for val in out if (val[0] == 100)])[:, 4]*0.001, \
                'Channel Nr. [-]', 'Total Current [A]', 'IV ' + self.id, fn="iv_total_current_all_channels_100V_%s.png" % self.id)
        if (1000 in np.array(out)[:, 0]):
            self.print_graph(np.array([val for val in out if (val[0] == 1000)])[:, 1], \
                np.array([val for val in out if (val[0] == 1000)])[:, 2], \
                np.array([val for val in out if (val[0] == 1000)])[:, 3], \
                'Channel Nr. [-]', 'Leakage Current [A]', 'IV ' + self.id, fn="iv_all_channels_1000V_%s.png" % self.id)
            self.print_graph(np.array([val for val in out if (val[0] == 100)])[:, 1], \
                np.array([val for val in out if (val[0] == 1000)])[:, 4], \
                np.array([val for val in out if (val[0] == 1000)])[:, 4]*0.001, \
                'Channel Nr. [-]', 'Leakage Current [A]', 'IV ' + self.id, fn="iv_total_current_all_channels_1000V_%s.png" % self.id)
        self.logging.info("\n")


    def finalise(self):
        self._finalise()
