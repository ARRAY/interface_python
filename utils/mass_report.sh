#!/bin/bash

source $HOME/setup_user.sh

FILES_LW=(
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_120um_com_Z5471_13/HPK_8in_198ch_120um_com_Z5471_13_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_120um_com_Z5471_19/HPK_8in_198ch_120um_com_Z5471_19_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_120um_ind_Z5471_1/HPK_8in_198ch_Z5471_1_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_120um_ind_Z5471_6/HPK_8in_198ch_120um_ind_Z5471_6_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_200um_com_Z3415_3/1st_measurement/HPK_8in_198ch_200um_com_Z3415_3_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_200um_com_Z3415_3/2nd_measurement/HPK_8in_198ch_200um_com_Z3415_3_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_120um_ind_Z5471_7/HPK_8in_198ch_120um_com_Z5471_7_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_200um_com_Z3415_5/1stMeasurement/HPK_8in_198ch_200um_com_Z3415_5_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_200um_com_Z3415_5/2ndMeasurement/HPK_8in_198ch_200um_com_Z3415_5_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_200um_ind_Z3413_5/1stMeasurement/HPK_8in_198ch_200um_ind_Z3413_5_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_200um_ind_Z3413_5/2ndMeasurement/HPK_8in_198ch_200um_ind_Z3413_5_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_200um_ind_Z3413_5/3rdMeasurement/HPK_8in_198ch_200um_ind_Z3413_5_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_200um_ind_Z3413_7/HPK_8in_198ch_200um_ind_Z3413_7_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_200um_ind_Z3413_9/1stMeasurement/HPK_8in_198ch_200um_ind_Z3413_9_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_200um_ind_Z3413_9/2ndMeasurement/HPK_8in_198ch_200um_ind_Z3413_9_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_300um_com_Z3415_2/HPK_8in_198ch_300um_com_Z3415_2_GR_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_300um_com_Z3415_4/1stMeasurement/HPK_8in_198ch_300um_com_Z3415_4_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_300um_com_Z3415_4/2ndMeasurement/HPK_8in_198ch_300um_com_Z3415_4_IV.tx"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_300um_ind_Z3413_4/HPK_8in_198ch_300um_ind_Z3413_4_GR_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_300um_ind_Z3413_6/HPK_8in_198ch_300um_ind_Z3413_6_IV.txt"
	"/Volumes/Media/Data/hgc/sensor_testing/cern/probecard/HPK_8in_198ch_300um_ind_Z3415_4/HPK_8in_198ch_300um_ind_Z3415_4_IV.txt"
)

FILES_PY=(
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_120_Z5471_2/iv_2nd_multimeter/iv.dat"
)

FILES_PY_OLD=(
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_120_Z5471_2/iv_1st_electrometer/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_120_Z5471_5/iv/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_120_Z5471_17/iv/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_120_Z5471_18/iv_1st_compl_1mA/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_120_Z5471_18/iv_2nd_compl_5mA/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3415_15/iv/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3415_1/iv/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3415_11/iv_1st/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3415_11/iv_2nd/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3415_11/iv_3rd/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3414_5/iv_1st/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3414_5/iv_2nd/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3414_5/iv_2nd/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3414_5/iv_2nd/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3414_5/iv_1st/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3414_3/iv_1st/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3414_3/iv_2nd/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3414_3/iv_3rd/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3414_3/iv_4th/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3414_3/iv_5th/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_200_Z3414_3/iv_6th/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_300_Z3413_8/iv/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_300_Z3413_10/iv/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_300_Z3415_10/iv/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_300_Z3415_8/iv_1st/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_300_Z3415_8/iv_2nd/iv.dat"
	"/Volumes/Media/Data/hgc/sensor_testing/hephy/probecard/HPK_8in_192ch_300_Z3415_8/iv_3rd/iv.dat"
)


# Python recordings
# for F in "${FILES_PY[@]}"
# do
# 	python report_iv.py -i ${F} -g ../config/geometry8Inch192.txt --typ ld
# done

# Python old recordings
for F in "${FILES_PY_OLD[@]}"
do
	python report_iv_python_old.py -i ${F} -g ../config/geometry8Inch192.txt --typ ld
done

# Lawbview recordings
# for F in "${FILES_LW[@]}"
# do
# 	python report_iv.py -i ${F} -g ../config/geometry8Inch192.txt --typ ld -s 1e-9
# done
