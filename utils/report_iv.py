#!/usr/bin/python3

# ============================================================================
# File: report_iv.py
# ------------------------------
#
# Notes:
#   - clusters are defined via the geo file for hexplot
#   - cells with distance in x or y of less than 0.8
#
# Layout:
#   
#
# Status:
#   work in progress
#
# ============================================================================

import os
import sys
import math
import shlex
import subprocess
import numpy as np
from cmath import *
from optparse import OptionParser



def call_hexplot(dat_file, geo_file, scale):
    print("-- Making plots.\n")
    for v in [10, 25, 50, 75, 100, 200, 500, 600, 800, 1000]:
        out_path = '/'.join(dat_file.split('/')[:-1]) + '/fig_wafer_%dV.pdf' % v 
        cmd = 'hexplot -p GEO -i %s -g %s -o %s ' % (dat_file, geo_file, out_path)
        cmd += '--invertVal --select -%d -z -0.5:100 --ys %.2e ' % (v, 1e9*scale)
        cmd += '--if SELECTOR:PADNUM:VAL --IV --colorpalette 57 '
        cmd += '--info tl:%s:tr:%s' % ('cell~current', dat_file.split('/')[-3])
        subprocess.call(shlex.split(cmd))

    for v in [1]:
        out_path = '/'.join(dat_file.split('/')[:-1]) + '/fig_cells.pdf' 
        cmd = 'hexplot -p FLAT -i %s -g %s -o %s ' % (dat_file, geo_file, out_path)
        cmd += '--invertVal --invertx -z -0.5:100 --ys 1e9 '
        cmd += '--if SELECTOR:PADNUM:VAL --IV --colorpalette 57 '
        cmd += '--info tl:%s:tr:%s --detailed' % ('cell~current', dat_file.split('/')[-3])
        subprocess.call(shlex.split(cmd))

    return 0


def process_file(dat_file, geo_file, scale, typ, species):
    
    dat = np.genfromtxt(dat_file, dtype='float', comments='#')
    geo = np.genfromtxt(geo_file, dtype='float', comments='#')
    
    report = "-- Report on data from\n-- %s" % dat_file
    
    if typ == "ld":
        pass
    elif typ == "hd":
        pass
    else:
        print("No valid sensor type specified. Choose ld or hd.")
        return -1

    if species == "full":
        nbad_max = 8
    elif species == "half":
        nbad_max = 4
    elif species == "choptwo":
        nbad_max = 6
    elif species == "chopfour":
        nbad_max = 2
    else:
        print("No valid sensor species specified. Choose 'full', 'half', 'choptwo' or 'chopfour'.")
        return -1

    ## columns of the relevant values in the data format
    lvolts = 0
    lchans = 1
    lvals = 2
    lvalstot = 4


    ## scale everything to [A] and take absolute value
    for d in dat:
        if (d[lvals] == 9.9E+46) or (abs(d[lvals]) == 1.0E+6):
            d[lvals] = np.nan
            
        d[lvolts] = abs(d[lvolts])
        d[lvals] = abs(d[lvals]*scale)
        d[lvalstot] = abs(d[lvalstot]*scale)

    ## get volts and channels
    volts = np.array([d for d in dat if d[lchans] == dat[0, lchans]])[:, lvolts]
    chans = np.array([d for d in dat if d[lvolts] == dat[0, lvolts]])[:, lchans]

    nvolts = len(volts)
    nchans = len(chans)
    print(nchans, nvolts)
    
    ## skip last voltage steps if not complete
    if len([d for d in dat if d[lchans] == dat[-1, lchans]]) != nchans:
        volts = np.delete(volts, -1, 0)
        nvolts = len(volts)
        print(nchans, nvolts)

    c_bad = []
    v_max = volts[-1]
    i_tot = np.zeros(nvolts)
    i_sum = np.zeros(nvolts)
    
    
    ## check evaluation criteria per channel
    i_600_tot = i_800_tot = i_max_tot = 0
    for c in chans:
        vals = np.array([d for d in dat if d[lchans] == c])[:, lvals]
        vals_tot = np.array([d for d in dat if d[lchans] == c])[:, lvalstot]

        i_600 = i_800 = i_max = 0
        for j in range(nvolts):
            i_sum[j] += vals[j]
            i_tot[j] += vals_tot[j]
            if volts[j] == 600:
                i_600 = vals[j]
            if volts[j] == 800:
                i_800 = vals[j]
            if volts[j] == v_max: 
                i_max = vals[j]
        
        if typ == "ld" and c == 199:
            continue
        elif typ == "hd" and c == 446:
            continue
        
        if (v_max < 600) and ((i_max > 1.E-7) or (math.isnan(i_max))):
            c_bad.append(c)
        elif (i_600 > 1.E-7) or (math.isnan(i_600)):
            c_bad.append(c)
        elif (i_800 > 2.5*i_600 and i_800 > 1.E-9) or (math.isnan(i_800)):
            c_bad.append(c)
        else:
            pass


    ## check evaluation criteria per sensor
    report += "\n\n-- # Bias [V]\tCurrent Sum [A]\tTotal Current [A]"
    for j in range(nvolts):
        i_tot[j] /= nchans
        if volts[j] == 600:
            i_600_tot = i_tot[j]
        if volts[j] == 800:
            i_800_tot = i_tot[j]
        if volts[j] == v_max: 
            i_max_tot = i_tot[j]

        report += "\n-- %5d\t%8.2e\t%8.2e" % (volts[j], i_sum[j], i_tot[j])


    ## look for clusters of bad cells
    cs_bad = []

    for c in c_bad:
        x,y,t = np.array([g for g in geo if g[0] == c])[0, 1:4]
        cs_bad.append(0)
        for c2 in c_bad:
            x2,y2 = np.array([g for g in geo if g[0] == c2])[0, 1:3]
            if (abs(x-x2) < 0.8) and (abs(y-y2) < 0.8):
                cs_bad[-1] += 1

    
    ## evaluate and report    
    report += "\n-- The sensor has %d bad cells. Those are:" % len(c_bad)
    report += "\n-- [" + ", ".join(str(int(c)) for c in c_bad) + "]"
    report += "\n-- The largest number of adjacend bad cells is %d." % max(cs_bad, default=0)
    
    if (v_max < 600):
        report += "\n-- The maximum bias voltage reached is %d V with a total current of %.2e A.\n\n" % (v_max, i_max_tot)
    elif (v_max < 800):
        report += "\n-- The maximum bias voltage reached is %d V. The total current at 600V is %.2e A.\n\n" % (v_max, i_600_tot)
    else:
        report += "\n-- The total current at 600V is %.2e A and at 800V %.2e A.\n\n" % (i_600_tot, i_800_tot)

    if (len(c_bad) > nbad_max):
        report += "\n-- Verdict: This is a BAD sensor."
        report += "\n-- Reason: To many bad cells.\n\n"
    elif (i_600_tot > 1.E-4) or (v_max < 600):
        report += "\n-- Verdict: This is a BAD sensor."
        report += "\n-- Reason: Total current too large.\n\n"      
    elif any(cs > 2 for cs in cs_bad):
        report += "\n-- Verdict: This is a BAD sensor."
        report += "\n-- Reason: Clusters of bad cells.\n\n"
    else:
        report += "\n-- Verdict: This is a GOOD sensor.\n\n"
        
    f = open('/'.join(dat_file.split('/')[:-1]) + '/report.txt', 'w') 
    f.write(report) 
    f.close()
    print(report)
    
    return 0



# Main Executable
def main():
    usage = "usage: ./report_iv.py -i [input_file] -g [geometry_file] --typ [sensor_type]"

    parser = OptionParser(usage=usage, version="0.1")
    parser.add_option("-i", "--input", action="store", dest="dat_file", type="string", help="input file")
    parser.add_option("-g", "--geo", action="store", dest="geo_file", type="string", help="geo file")
    parser.add_option("-s", "--scale", action="store", dest="scale", type="float", default=1, help="scale the current measurement if units are not [A]")
    parser.add_option("--ex", "--examples", action="store_true", dest="fExamples", help="print examples")
    parser.add_option("--typ", "--typ", action="store", dest="typ", type="string", help="sensor typ (ld or hd)")
    parser.add_option("--species", "--species", action="store", dest="species", type="string", default="full", help="sensor species (full, half, choptwo or chopfour)")

    (options, args) = parser.parse_args()

    if options.fExamples:
        print('\nExample commands for running this script:\n')
        print(''' ./report_iv.py -i ../logs/test/iv.dat -g geometry8Inch192.txt --typ ld''')
        print(''' ./report_iv.py -i ../logs/test/iHPK_8in_432ch_Z3510.txt -g geometry8Inch432.txt --typ hd -s 1e-9''')
        print(''' ./report_iv.py -i ../logs/test/iv.dat -g geometry8Inch192.txt --typ ld --species choptwo''')
        print('\nNotes on running:\n')
        print(' Current data recorded with LabVIEW needs to be scaled via -s 1e-9.')

    else:
        ret = process_file(options.dat_file, options.geo_file, \
            options.scale, options.typ, options.species)
        ret = call_hexplot(options.dat_file, options.geo_file, options.scale)
        


if __name__ == "__main__":
    main()
